package brute_force;

public class BruteForce {
    public static void main(String[] args) {
        String source = "AABAACAADAABAAABAA";
        String pattern = "AABA";
        int srcLength = source.length();
        int patLength = pattern.length();
        for (int i = 0; i <= srcLength - patLength; i++) {
            int j;
            for (j = 0; j < patLength; j++) {
                if (source.charAt(i + j) != pattern.charAt(j)) break;
            }
            if (j == patLength) {
                System.out.println("found at position" + i);
            }
        }
    }
}
