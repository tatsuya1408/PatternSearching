package not_so_naive;

import java.util.Arrays;

public class NotSoNaive {

    private static boolean compareString(String s1, String s2) {
        if (s1.length() != s2.length()) return false;
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) != s2.charAt(i)) return false;
        }
        return true;
    }

    private static void searchByString(String pattern, String src) {
        int u = 1, v = 2; // buoc nhay
        int i = 0;
        String x1 = pattern.substring(2, pattern.length());
        if (pattern.charAt(0) == pattern.charAt(1)) {
            u = 2;
            v = 1;
        }
        while (i <= src.length() - pattern.length()) {
            if (pattern.charAt(1) != src.charAt(i + 1)) {
                i += u;
            } else {
                if ((pattern.charAt(0) == src.charAt(i))
                        && compareString(x1, src.substring(i + 2, i + pattern.length()))
                        ) {
                    System.out.println("found at position " + i);
                }
                i += v;
            }
        }
    }

    public static void main(String[] args) {
        String pattern = "GCAGAGAG";
        String src = "GCATCGCAGAGAGTATACAGTACG";
        searchByString(pattern, src);
    }
}
