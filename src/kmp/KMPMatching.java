package kmp;

public class KMPMatching {

    private static void KMPSearch(String pattern, String src) {
        int patternLength = pattern.length();
        int srcLength = src.length();

        // lps[] will hold the longest prefix suffix values for pattern
        int lps[] = new int[patternLength];
        int j = 0; // index for pattern[]
        int i = 0; // index for src[]

        // Preprocess the pattern (calculate lps[] array)
        preProcess(pattern, lps);

        while (i < srcLength) {
            if (pattern.charAt(j) == src.charAt(i)) {
                j++;
                i++;
            }
            if (j == patternLength) {
                System.out.println("Found pattern " + "at index " + (i - j));
                j = lps[j - 1];
            }

            // mismatch after j matches
            else if (i < srcLength && pattern.charAt(j) != src.charAt(i)) {
                if (j != 0) {
                    j = lps[j - 1];
                } else {
                    i = i + 1;
                }
            }
        }
    }

    private static void preProcess(String pattern, int lps[]) {
        // length of the previous longest prefix suffix
        int len = 0;
        int i = 1;
        lps[0] = 0; // lps[0] is always 0

        int patternLength = pattern.length();
        while (i < patternLength) {
            if (pattern.charAt(i) == pattern.charAt(len)) {
                len++;
                lps[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = lps[len - 1];
                } else {
                    lps[i] = len;
                    i++;
                }
            }
        }
    }

    public static void main(String args[]) {
        String txt = "abcdab abcdabcdabde";
        String pat = "abcdabd";
        KMPSearch(pat, txt);
    }
}
